# Unnamed Framework #

So many frameworks to choose from these days, which do you choose? Well I had that same question and after using many for the past 15 years I decided to make one that fits my needs at the most basic level. I use to use a heavily modified version of Codeigniter for a long time, but when it stopped being worked I had to move on. I evaluated the latest and greatest of the big ones (Drupal, DooPHP, Symfony, Zend, Larvel, CakePHP) and found them either bloated, wanting, or just plain terrible for my needs. Larvel is by far my favorite of them all. Though its not "enterprise level" it is definitely one to keep an eye on as far as becoming THE framework to use.

Too many frameworks these days are built to cater to the non-programmer. Making things easier to use is great, but when you are in fact a programmer using abstracted functionality and non-native syntax to achieve your goal might make you want to crawl in a hole.

I built this because I wanted to stay as native as possible to the language I develop in (PHP), the database I use mainly (MySQL), and the libraries I use often (Bootstrap, jQuery, Handlebars, MomentJS, and Twig). I chose to use Symfony Components (HTTPFoundation and Routing) because in my opinion are very solid libraries. My philosophy on what a framework should be is, a set of tools, functionality, methodologies, and constructs that are close to the framework's native language as possible. It should be fast and able to deliver a request in under 300ms. 

I use MySQL 99.999999% of the time, so there is no database abstraction layer. I also dont use an ORM. ORMs just aren't where they need to be yet. Most of them cause a lot of unoptimized queries and have issues with complex joins. I have hand written my database to model interaction by hand for so long that I would like to CRUD solution that doesn't use too much magic (still looking). I often use data storing strategies that are outside the standard CRUD model. 

This framework is far from perfect or comeplete. I would like to find a logging system that works well (Monolog). I would also like to find a profiler that doesn't add too much overhead (still looking).

# Generate Documentation #

`
phpdoc -d c:/wamp/www/symfony-based-framework -t c:/wamp/www/symfony-based-framework/doc --ignore "vendor/*, doc/*" --template="responsive-twig"
`