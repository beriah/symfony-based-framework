<?php
/**
 * App Class Definition File
 */
namespace TGCore;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class App
 *
 * @package TGCore
 */
class App {
	/**
	 * @var Request
	 */
	protected $request;
	/**
	 * @var Response
	 */
	protected $response;
	/**
	 * @var string
	 */
	protected $environment;

	/**
	 * constructor
	 *
	 * @param Request $request
	 * @param Response $response
	 * @param string $environment
	 */
	public function __construct( Request $request, Response $response, $environment = ENVIRONMENT )
	{
		$this->request = $request;
		$this->response = $response;
		$this->environment = $environment;
	}

	/**
	 * @return Response
	 */
	public function getResponse()
	{
		return $this->response;
	}

	/**
	 * @return Request
	 */
	public function getRequest()
	{
		return $this->request;
	}
}