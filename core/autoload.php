<?php

spl_autoload_register(
	function( $class )
	{
		$filename = ltrim( strtolower( $class ), 'tg' );
		include ROOT_PATH . "/{$filename}.php";
	}
);
