<?php

namespace TGCore;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

define( 'ROOT_PATH', realpath( __DIR__ . '/../' ) );
define( 'CORE_PATH', realpath( ROOT_PATH . '/core/' ) );
define( 'APP_PATH', realpath( ROOT_PATH . '/app/' ) );

require_once ROOT_PATH . '/vendor/autoload.php';
require_once ROOT_PATH . '/_environment.php';

// load config (namespace that maps to core, namespace that maps to app)

require_once CORE_PATH . '/autoload.php';

$app = new App( Request::createFromGlobals(), new Response(), ENVIRONMENT );
$app->getResponse()->send();
